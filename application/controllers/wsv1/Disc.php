<?php
header('Access-Control-Allow-Origin: *');

defined('BASEPATH') OR exit('No direct script access allowed');

class Disc extends CI_Controller {

  ### DISCIPLINARY ###
    public function __construct() {
      header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Methods: GET, POST");
  parent::__construct();
}

	public function viewAllDisc()
	{
    // SELECT a.SI_ID, CONCAT(a.si_LNAME, ', ',  a.si_FNAME , ' ' , a.si_MNAME) as FULLNAME, b.DISC_ACTION, b.DATE FROM student_information as a INNER JOIN  disc_action as b WHERE a.si_ID = b.si_ID
   $data = $this->DiscModel->viewAllDisc();
   foreach($data as $row){
     $arr[] = array(
       "SI_ID" => $row->SI_ID,
       "DISC_ID" => $row->DISC_ID,
       "FULLNAME" => $row->FULLNAME,
       "DISC_ACTION" => $row->DISC_ACTION,
       "DATA" => $row->DATE,
     );
    }  
    $response = array (
      'status' => 'SUCCESS',
      'message' => 'SUCCESS FETCHING',
      'payload' => $arr
    );
    echo json_encode($response);
  }
  
	public function viewStudents()
	{
   $data = $this->DiscModel->viewStudents();
   foreach($data as $row){
     $arr[] = array(
       "SI_ID" => $row->si_ID,
       "FULLNAME" => $row->FULLNAME
     );
    }  
    $response = array (
      'status' => 'SUCCESS',
      'message' => 'SUCCESS FETCHING',
      'payload' => $arr
    );
    echo json_encode($response);
	}

  public function addDiscStudent()
  {
        $payload = json_decode(file_get_contents('php://input'),true);
    $data = $this->DiscModel->addDiscStudent($payload);
    echo $data;
  }

  public function dltDiscStudent()
  {
    $payload = json_decode(file_get_contents('php://input'),true);
    $this->DiscModel->dltDiscStudent($payload);
  }

}