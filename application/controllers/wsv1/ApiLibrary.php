<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiLibrary extends CI_Controller {


	public function __construct() {
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
    	header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

    	parent::__construct();
	}

	public function getallBookByBookStatus()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->getallBookByBookStatus($payload);
	}

	public function insert_reg_library()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->insert_reg_library($payload);
	}

	public function insert_borrow_returned()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->insert_borrow_returned($payload);
	}

	public function insertMaterials()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->insertMaterials($payload);
	}

	public function insertAuthor()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->insertAuthor($payload);
	}

	public function getLibraryPurposeByID()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->getLibraryPurposeByID($payload);
	}

	public function getreg_libraryByID()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->getreg_libraryByID($payload);
	}

	public function getLibraryTypeID()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->getLibraryTypeID($payload);
	}

	public function getResourceTypeID()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->getResourceTypeID($payload);
	}

	public function getBookStatusByID()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->getBookStatusByID($payload);
	}

	public function getAuthorbyID()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->getAuthorbyID($payload);
	}

	public function getBookBorrowedByID()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->getBookBorrowedByID($payload);
	}

	public function getMaterialbyID()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->getMaterialbyID($payload);
	}

	public function fetchallreg_library()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->fetchallreg_library($payload);
	}

	public function fetchAllBookStatus()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->fetchAllBookStatus($payload);
	}

	public function fetchAllLibraryPurpose()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->fetchAllLibraryPurpose($payload);
	}

	public function fetchAllLibraryType()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->fetchAllLibraryType($payload);
	}

	public function fetchAllResourceType()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->fetchAllResourceType($payload);
	}

	public function fetchAllBookBorrowedType()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->fetchAllBookBorrowedType($payload);
	}

	public function fetchAllMaterials()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->fetchAllMaterials($payload);
	}

	public function fetchAllMaterials2()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->fetchAllMaterials2($payload);
	}

	public function updateMaterials()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->updateMaterials($payload);
	}

	public function updateAuthor()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->updateAuthor($payload);
	}

	public function updateBorrow_returned()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->updateBorrow_returned($payload);
	}

		public function fetchallrequest()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->V1_model->fetchallrequest($payload);
	}

	public function RequestExpiration(){
		echo  $this->V1_model->RequestExpiration();
	}

}
