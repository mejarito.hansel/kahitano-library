<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {


	public function __construct() {
        header('Access-Control-Allow-Origin: *');
    	header("Access-Control-Allow-Methods: GET, POST");
    	parent::__construct();
	}

	public function index()
	{
		$this->load->view('version');
	}


	### STUDENT ###
	public function Student()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->ApiV1_model->student($payload);
	}
	### Dorm ###
		public function Dorm()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->ApiDormv1_model->dorm($payload);
	}
		### AUDIT ###
	public function Audit()
	{
		$payload = json_decode(file_get_contents('php://input'));
		echo $this->ApiV1_model->student($payload);
	}
}