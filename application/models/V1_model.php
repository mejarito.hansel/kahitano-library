<?php 
  
  class V1_model extends CI_Model
  {

  public function getDb2() {
        $CI = &get_instance();
        $this->DB2 = $CI->load->database('seconddb', TRUE);

  }

    public function fetchAllLibraryPurpose($payload){ //VISITOR_LIBRARY_PURPOSE
     header('Content-Type: application/json');
     $this->getDb2();

     $sql = $this->DB2->get('VISITOR_LIBRARY_PURPOSE'); //could be any table
             if($sql){
               if($sql->num_rows()>0) {
                   $data = $sql->result();
                   $response = array('status' => 'SUCCESS',
                                      'message' => 'SUCCESS FETCHING DATA',
                                      'payload' => $data);
                  
                   return json_encode($response);
                }
               else{
                   $response = array('status' => 'FAILED',
                                      'message' => 'FAILED RETRIEVING DATA');
                    return json_encode($response);
               }        
            }
             else{
               $response = array('status' => 'ERROR',
                                 'message' => 'ERROR');
                return json_encode($response);            
                $error = $this->DB2->error();
                print_r($error);
            }
    }

    public function getLibraryPurposeByID($payload){ //getLibraryPurposeByIDasd  
     header('Content-Type: application/json');
     $this->getDb2();
        $this->DB2->where('VISITOR_LIBRARY_PURPOSE_ID', $payload->VISITOR_LIBRARY_PURPOSE_ID);
            $query = $this->DB2->get('VISITOR_LIBRARY_PURPOSE');
           
             $result = $query->row();
            
            if($query->num_rows() == 1){
               $response = array('status' => 'SUCCESS',
                                 'message' => 'SUCCESS FETCHING DATA',
                                  'payload' => $result); // if success
               
               return json_encode($response);
            }
             else{
               $response = array('status' => 'FAILED',
                                 'message' => 'FAILED FETCHING DATA'); // if failed
               return json_encode($response);
           }
    } 

    public function fetchAllLibraryType($payload){ //VISITOR_LIBRARY_TYPE
     header('Content-Type: application/json');
     $this->getDb2();

     $sql = $this->DB2->get('VISITOR_LIBRARY_TYPE'); //could be any table
             if($sql){
               if($sql->num_rows()>0) {
                   $data = $sql->result();
                   $response = array('status' => 'SUCCESS',
                                      'message' => 'SUCCESS FETCHING DATA',
                                      'payload' => $data);
                  
                   return json_encode($response);
                }
               else{
                   $response = array('status' => 'FAILED',
                                      'message' => 'FAILED RETRIEVING DATA');
                    return json_encode($response);
               }        
            }
             else{
               $response = array('status' => 'ERROR',
                                 'message' => 'ERROR');
                return json_encode($response);            
                $error = $this->DB2->error();
                print_r($error);
            }
    }

    public function getMaterialbyID($payload){ //VISITOR_LIBRARY_TYPE  
     header('Content-Type: application/json');
     $this->getDb2();
        $this->DB2->where('MATERIAL_ID', $payload->MATERIAL_ID);
            $query = $this->DB2->get('MATERIALS');
           
             $result = $query->row();
            
            if($query->num_rows() == 1){
               $response = array('status' => 'SUCCESS',
                                 'message' => 'SUCCESS FETCHING DATA',
                                  'payload' => $result); // if success
               
               return json_encode($response);
            }
             else{
               $response = array('status' => 'FAILED',
                                 'message' => 'FAILED FETCHING DATA'); // if failed
               return json_encode($response);
           }
    } 

    public function getAuthorbyID($payload){ //AUTHOR  
     header('Content-Type: application/json');
     $this->getDb2();
        $this->DB2->where('AUTHOR_ID', $payload->AUTHOR_ID);
            $query = $this->DB2->get('AUTHOR');
           
             $result = $query->row();
            
            if($query->num_rows() == 1){
               $response = array('status' => 'SUCCESS',
                                 'message' => 'SUCCESS FETCHING DATA',
                                  'payload' => $result); // if success
               
               return json_encode($response);
            }
             else{
               $response = array('status' => 'FAILED',
                                 'message' => 'FAILED FETCHING DATA'); // if failed
               return json_encode($response);
           }
    } 

    public function getLibraryTypeID($payload){ //VISITOR_LIBRARY_TYPE  
     header('Content-Type: application/json');
     $this->getDb2();
        $this->DB2->where('VISITOR_LIBRARY_TYPE_ID', $payload->VISITOR_LIBRARY_TYPE_ID);
            $query = $this->DB2->get('VISITOR_LIBRARY_TYPE');
           
             $result = $query->row();
            
            if($query->num_rows() == 1){
               $response = array('status' => 'SUCCESS',
                                 'message' => 'SUCCESS FETCHING DATA',
                                  'payload' => $result); // if success
               
               return json_encode($response);
            }
             else{
               $response = array('status' => 'FAILED',
                                 'message' => 'FAILED FETCHING DATA'); // if failed
               return json_encode($response);
           }
    } 

    public function fetchAllMaterials($payload){ //MATERIALS
     header('Content-Type: application/json');
     $this->getDb2();

     // $sql = $this->DB2->query("CALL viewMaterials()");
     $sql = $this->DB2->get('MATERIALS'); //could be any table
             if($sql){
               if($sql->num_rows()>0) {
                   $data = $sql->result();
                   $response = array('status' => 'SUCCESS',
                                      'message' => 'SUCCESS FETCHING DATA',
                                      'payload' => $data);
                  
                   return json_encode($response);
                }
               else{
                   $response = array('status' => 'FAILED',
                                      'message' => 'FAILED RETRIEVING DATA');
                    return json_encode($response);
               }        
            }
             else{
               $response = array('status' => 'ERROR',
                                 'message' => 'ERROR');
                return json_encode($response);            
                $error = $this->DB2->error();
                print_r($error);
            }
    } 

    public function fetchAllMaterials2($payload){ //MATERIALS
     header('Content-Type: application/json');
     $this->getDb2();

     $sql = $this->DB2->query("CALL viewMaterials()");
     // $sql = $this->DB2->get('MATERIALS'); //could be any table
             if($sql){
               if($sql->num_rows()>0) {
                   $data = $sql->result();
                   $response = array('status' => 'SUCCESS',
                                      'message' => 'SUCCESS FETCHING DATA',
                                      'payload' => $data);
                  
                   return json_encode($response);
                }
               else{
                   $response = array('status' => 'FAILED',
                                      'message' => 'FAILED RETRIEVING DATA');
                    return json_encode($response);
               }        
            }
             else{
               $response = array('status' => 'ERROR',
                                 'message' => 'ERROR');
                return json_encode($response);            
                $error = $this->DB2->error();
                print_r($error);
            }
    } 

    public function fetchallreg_library($payload){ //RESOURCE_TYPE
     header('Content-Type: application/json');
     $this->getDb2();

     $sql = $this->DB2->get('tbl_reg_library'); //could be any table
             if($sql){
               if($sql->num_rows()>0) {
                   $data = $sql->result();
                   $response = array('status' => 'SUCCESS',
                                      'message' => 'SUCCESS FETCHING DATA',
                                      'payload' => $data);
                  
                   return json_encode($response);
                }
               else{
                   $response = array('status' => 'FAILED',
                                      'message' => 'FAILED RETRIEVING DATA');
                    return json_encode($response);
               }        
            }
             else{
               $response = array('status' => 'ERROR',
                                 'message' => 'ERROR');
                return json_encode($response);            
                $error = $this->DB2->error();
                print_r($error);
            }
    } 

    public function fetchAllResourceType($payload){ //RESOURCE_TYPE
     header('Content-Type: application/json');
     $this->getDb2();

     $sql = $this->DB2->get('RESOURCE_TYPE'); //could be any table
             if($sql){
               if($sql->num_rows()>0) {
                   $data = $sql->result();
                   $response = array('status' => 'SUCCESS',
                                      'message' => 'SUCCESS FETCHING DATA',
                                      'payload' => $data);
                  
                   return json_encode($response);
                }
               else{
                   $response = array('status' => 'FAILED',
                                      'message' => 'FAILED RETRIEVING DATA');
                    return json_encode($response);
               }        
            }
             else{
               $response = array('status' => 'ERROR',
                                 'message' => 'ERROR');
                return json_encode($response);            
                $error = $this->DB2->error();
                print_r($error);
            }
    } 

    public function getreg_libraryByID($payload){ //RESOURCE_TYPE  
     header('Content-Type: application/json');
     $this->getDb2();
        $this->DB2->where('RL_ID', $payload->RL_ID);
            $query = $this->DB2->get('tbl_reg_library');
           
             $result = $query->row();
            
            if($query->num_rows() == 1){
               $response = array('status' => 'SUCCESS',
                                 'message' => 'SUCCESS FETCHING DATA',
                                  'payload' => $result); // if success
               
               return json_encode($response);
            }
             else{
               $response = array('status' => 'FAILED',
                                 'message' => 'FAILED FETCHING DATA'); // if failed
               return json_encode($response);
           }
    } 

    public function getResourceTypeID($payload){ //RESOURCE_TYPE  
     header('Content-Type: application/json');
     $this->getDb2();
        $this->DB2->where('RESOURCE_TYPE_ID', $payload->RESOURCE_TYPE_ID);
            $query = $this->DB2->get('RESOURCE_TYPE');
           
             $result = $query->row();
            
            if($query->num_rows() == 1){
               $response = array('status' => 'SUCCESS',
                                 'message' => 'SUCCESS FETCHING DATA',
                                  'payload' => $result); // if success
               
               return json_encode($response);
            }
             else{
               $response = array('status' => 'FAILED',
                                 'message' => 'FAILED FETCHING DATA'); // if failed
               return json_encode($response);
           }
    } 

    public function fetchAllBookBorrowedType($payload){ //BOOK_BORROWED_TYPE
     header('Content-Type: application/json');
     $this->getDb2();

     $sql = $this->DB2->get('BOOK_BORROWED_TYPE'); //could be any table
             if($sql){
               if($sql->num_rows()>0) {
                   $data = $sql->result();
                   $response = array('status' => 'SUCCESS',
                                      'message' => 'SUCCESS FETCHING DATA',
                                      'payload' => $data);
                  
                   return json_encode($response);
                }
               else{
                   $response = array('status' => 'FAILED',
                                      'message' => 'FAILED RETRIEVING DATA');
                    return json_encode($response);
               }        
            }
             else{
               $response = array('status' => 'ERROR',
                                 'message' => 'ERROR');
                return json_encode($response);            
                $error = $this->DB2->error();
                print_r($error);
            }
    } 

    public function fetchAllBookStatus($payload){ //BOOK_STATUS
     header('Content-Type: application/json');
     $this->getDb2();

     $sql = $this->DB2->get('BOOK_STATUS'); //could be any table
             if($sql){
               if($sql->num_rows()>0) {
                   $data = $sql->result();
                   $response = array('status' => 'SUCCESS',
                                      'message' => 'SUCCESS FETCHING DATA',
                                      'payload' => $data);
                  
                   return json_encode($response);
                }
               else{
                   $response = array('status' => 'FAILED',
                                      'message' => 'FAILED RETRIEVING DATA');
                    return json_encode($response);
               }        
            }
             else{
               $response = array('status' => 'ERROR',
                                 'message' => 'ERROR');
                return json_encode($response);            
                $error = $this->DB2->error();
                print_r($error);
            }
    } 

    public function getBookBorroweDB2yID($payload){ //BOOK_BORROWED_TYPE  
     header('Content-Type: application/json');
     $this->getDb2();
        $this->DB2->where('BOOK_BORROWED_TYPE_ID', $payload->BOOK_BORROWED_TYPE_ID);
            $query = $this->DB2->get('BOOK_BORROWED_TYPE');

             $result = $query->row();

            if($query->num_rows() == 1){
               $response = array('status' => 'SUCCESS',
                                 'message' => 'SUCCESS FETCHING DATA',
                                  'payload' => $result); // if success
               
               return json_encode($response);
            }
             else{
               $response = array('status' => 'FAILED',
                                 'message' => 'FAILED FETCHING DATA'); // if failed
               return json_encode($response);
           }
    } 

    public function getBookStatusByID($payload){ //BOOK_BORROWED_TYPE  
     header('Content-Type: application/json');
     $this->getDb2();
        $this->DB2->where('BOOK_STATUS_ID', $payload->BOOK_STATUS_ID);
            $query = $this->DB2->get('BOOK_STATUS');

             $result = $query->row();

            if($query->num_rows() == 1){
               $response = array('status' => 'SUCCESS',
                                 'message' => 'SUCCESS FETCHING DATA',
                                  'payload' => $result); // if success
               
               return json_encode($response);
            }
             else{
               $response = array('status' => 'FAILED',
                                 'message' => 'FAILED FETCHING DATA'); // if failed
               return json_encode($response);
           }
    }  

    public function insert_reg_library($payload){ //tbl_reg_library
      header('Content-Type: application/json');
     $this->getDb2();
      $datenow = date("Y-m-d H:i:s");
      $data = array("VISITOR_LIBRARY_TYPE_ID" => $payload->VISITOR_LIBRARY_TYPE_ID,
              "RG_NO" => $payload->RG_NO,
              "ID" => $payload->ID,
              "VISITOR_LIBRARY_PURPOSE_ID" => $payload->VISITOR_LIBRARY_PURPOSE_ID,
              "VISITOR_PURPOSE_OTHERS" => $payload->VISITOR_PURPOSE_OTHERS,
              "RL_DATE_LOG" => $datenow
              );

      if($this->DB2->insert('tbl_reg_library', $data)){
        $response = array('status' => 'SUCCESS',
                'message' => 'Reg Library ADDED SUCCESSFULLY',
                 'RL_ID' =>  $this->DB2->insert_id()
                 );
        echo json_encode($response);
      }else{
        $response = array('status' => 'ERROR',
                  'message' => 'FAILED ADDING TERMS');

        $error = $this->DB2->error(); // Has keys 'code' and 'message'
                  echo "<BR><BR>";
                  print_r($error);
                  echo "<BR><BR>";

        echo json_encode($response);
      }
    }

    public function insert_borrow_returned($payload){ //tbl_reg_library
      header('Content-Type: application/json');
     $this->getDb2();
      $datenow = date("Y-m-d H:i");
      $data = array("RL_ID" => $payload->RL_ID,
              "MATERIAL_ID" => $payload->MATERIAL_ID,
              "B_DATE_BORROWED" => $payload->B_DATE_BORROWED,
              "B_DUE_DATE" => $payload->B_DUE_DATE,
              "B_DATE_RETURNED" => $payload->B_DATE_RETURNED,
              "B_FINES" => $payload->B_FINES,
              "BOOK_STATUS_ID" => $payload->BOOK_STATUS_ID,
              "DATE_REQUESTED" => $datenow
              );

      if($this->DB2->insert('tbl_borrow_returned', $data)){
        $response = array('status' => 'SUCCESS',
                'message' => 'Borrow_Returned ADDED SUCCESSFULLY',
                 'B_ID' =>  $this->DB2->insert_id()
                 );
        echo json_encode($response);
      }else{
        $response = array('status' => 'ERROR',
                  'message' => 'FAILED ADDING TERMS');

        $error = $this->DB2->error(); // Has keys 'code' and 'message'
                  echo "<BR><BR>";
                  print_r($error);
                  echo "<BR><BR>";

        echo json_encode($response);
      }
    }

    public function insertMaterials($payload){ //MATERIALS
      header('Content-Type: application/json');
     $this->getDb2();
      $datenow = date("Y-m-d H:i:s");
      $data = array("ACCESSION_NO" => $payload->ACCESSION_NO,
              "DATE_ACCESSION" => $payload->DATE_ACCESSION,
              "MATERIAL_TITLE" => $payload->MATERIAL_TITLE,
              "AUTHOR_NAME" => $payload->AUTHOR_NAME,
              "MATERIAL_COPYRIGHT" => $payload->MATERIAL_COPYRIGHT,
              "MATERIAL_SOURCE" => $payload->MATERIAL_SOURCE,
              "MATERIAL_DATE_RECEIVED" => $payload->MATERIAL_DATE_RECEIVED,
              "BOOK_TYPE" => $payload->BOOK_TYPE,
              "BOOK_STATUS" => $payload->BOOK_STATUS,
              "BOOK_INDEX" => $payload->BOOK_INDEX,
              "CALL_NO" => $payload->CALL_NO
              );

      if($this->DB2->insert('MATERIALS', $data)){
        $response = array('status' => 'SUCCESS',
                'message' => 'MATERIALS ADDED SUCCESSFULLY',
                 'MATERIAL_ID' =>  $this->DB2->insert_id()
                 );
        echo json_encode($response);
      }else{
        $response = array('status' => 'ERROR',
                  'message' => 'FAILED ADDING TERMS');

        $error = $this->DB2->error(); // Has keys 'code' and 'message'
                  echo "<BR><BR>";
                  print_r($error);
                  echo "<BR><BR>";

        echo json_encode($response);
      }
    }

    public function updateMaterials($payload){
      header('Content-Type: application/json');
     $this->getDb2();
      $datenow = date("Y-m-d H:i:s");
      $data = array("ACCESSION_NO" => $payload->ACCESSION_NO,
              "DATE_ACCESSION" => $payload->DATE_ACCESSION,
              "MATERIAL_TITLE" => $payload->MATERIAL_TITLE,
              "AUTHOR_NAME" => $payload->AUTHOR_NAME,
              "MATERIAL_COPYRIGHT" => $payload->MATERIAL_COPYRIGHT,
              "MATERIAL_SOURCE" => $payload->MATERIAL_SOURCE,
              "MATERIAL_DATE_RECEIVED" => $payload->MATERIAL_DATE_RECEIVED,
              "BOOK_TYPE" => $payload->BOOK_TYPE,
              "BOOK_STATUS" => $payload->BOOK_STATUS,
              "BOOK_INDEX" => $payload->BOOK_INDEX,
              "CALL_NO" => $payload->CALL_NO
              );

      $this->DB2->where('MATERIAL_ID',$payload->MATERIAL_ID);
      if($this->DB2->update('MATERIALS', $data)){
        $response = array('status' => 'SUCCESS',
                'message' => 'MATERIALS UPDATED SUCCESSFULLY'
                 );
        return json_encode($response);
      }else{
        $response = array('status' => 'ERROR',
                  'message' => 'FAILED UPDATING MATERIALS');

        $error = $this->DB2->error(); // Has keys 'code' and 'message'
                  echo "<BR><BR>";
                  print_r($error);
                  echo "<BR><BR>";

        return json_encode($response);
      }
    }

    public function updateBorrow_returned($payload){
      header('Content-Type: application/json');
     $this->getDb2();
      $datenow = date("Y-m-d H:i:s");
      $data = array("B_FINES" => $payload->B_FINES,
              "B_DATE_BORROWED" => $payload->B_DATE_BORROWED,
              "B_DUE_DATE" => $payload->B_DUE_DATE,
              "BOOK_STATUS_ID" => $payload->BOOK_STATUS_ID
              );

      $this->DB2->where('B_ID',$payload->B_ID);
      if($this->DB2->update('tbl_borrow_returned', $data)){
        $response = array('status' => 'SUCCESS',
                'message' => 'MATERIALS UPDATED SUCCESSFULLY'
                 );
        return json_encode($response);
      }else{
        $response = array('status' => 'ERROR',
                  'message' => 'FAILED UPDATING MATERIALS');

        $error = $this->DB2->error(); // Has keys 'code' and 'message'
                  echo "<BR><BR>";
                  print_r($error);
                  echo "<BR><BR>";

        return json_encode($response);
      }
    }

    public function insertAuthor($payload){ //AUTHOR
      header('Content-Type: application/json');
     $this->getDb2();
      $datenow = date("Y-m-d H:i:s");
      $data = array("AUTHOR_FNAME" => $payload->AUTHOR_FNAME,
              "AUTHOR_LNAME" => $payload->AUTHOR_FNAME,
              "AUTHOR_MNAME" => $payload->AUTHOR_MNAME
              );

      if($this->DB2->insert('AUTHOR', $data)){
        $response = array('status' => 'SUCCESS',
                'message' => 'AUTHOR ADDED SUCCESSFULLY',
                 'AUTHOR_ID' =>  $this->DB2->insert_id()
                 );
        echo json_encode($response);
      }else{
        $response = array('status' => 'ERROR',
                  'message' => 'FAILED ADDING TERMS');

        $error = $this->DB2->error(); // Has keys 'code' and 'message'
                  echo "<BR><BR>";
                  print_r($error);
                  echo "<BR><BR>";

        echo json_encode($response);
      }
    }
    
    public function updateAuthor($payload){
      header('Content-Type: application/json');
     $this->getDb2();
      $datenow = date("Y-m-d H:i:s");
      $data = array("AUTHOR_FNAME" => $payload->AUTHOR_FNAME,
              "AUTHOR_LNAME" => $payload->AUTHOR_FNAME,
              "AUTHOR_MNAME" => $payload->AUTHOR_MNAME
              );
      
      $this->DB2->where('AUTHOR_ID',$payload->AUTHOR_ID);
      if($this->DB2->update('AUTHOR', $data)){
        $response = array('status' => 'SUCCESS',
                'message' => 'AUTHOR UPDATED SUCCESSFULLY'
                 );
        return json_encode($response);
      }else{
        $response = array('status' => 'ERROR',
                  'message' => 'FAILED UPDATING AUTHOR');

        $error = $this->DB2->error(); // Has keys 'code' and 'message'
                  echo "<BR><BR>";
                  print_r($error);
                  echo "<BR><BR>";

        return json_encode($response);
      }
    }

    public function fetchallrequest($payload){ //MATERIALS
     header('Content-Type: application/json');
     $this->getDb2();
        $sql = $this->DB2->query("CALL fetchallBorrowedReturn()");
        // $sql = $this->DB2->query("CALL viewBookStatus()");
        // $sql = $this->DB2->query("CALL viewMaterials()");
             if($sql){
               if($sql->num_rows()>0) {
                   $data = $sql->result();
                   $response = array('status' => 'SUCCESS',
                                      'message' => 'SUCCESS FETCHING DATA',
                                      'payload' => $data);
                  
                   return json_encode($response);
                }
               else{
                   $response = array('status' => 'FAILED',
                                      'message' => 'FAILED RETRIEVING DATA');
                    return json_encode($response);
               }
            }
             else{
               $response = array('status' => 'ERROR',
                                 'message' => 'ERROR');
                return json_encode($response);            
                $error = $this->DB2->error();
                print_r($error);
            }
    }

    public function getallBookByBookStatus($payload){ //MATERIALS
     //header('Content-Type: application/json');

        $sql = $this->DB2->query('CALL fetchallBorrowedReturn("'.$payload["BOOK_STATUS_ID"].'")');
        // $sql = $this->db->query("CALL viewBookStatus()");
        // $sql = $this->db->query("CALL viewMaterials()");
             // $result = $sql->result();
             if($sql){
               if($sql->num_rows() > 0 ) {
                   $data = $sql->result();
                   $response = array('status' => 'SUCCESS',
                                      'message' => 'SUCCESS FETCHING DATA',
                                      'payload' => $data);
                  
                   return json_encode($response);
                }

               else{
                   $response = array('status' => 'FAILED',
                                      'message' => 'FAILED RETRIEVING DATA');
                    return json_encode($response);
               }
            }
            
             else{
               $response = array('status' => 'ERROR',
                                 'message' => 'ERROR');
                return json_encode($response);            
                $error = $this->DB2->error();
                print_r($error);
            }
    } 

    public function RequestExpiration(){
      header('Content-Type: application/json');
      $this->getDb2();
        $date = date("Y-m-j H:i", strtotime( '-1 days' ) ).":00";
        $this->DB2->set('BOOK_STATUS_ID', 5);
        $this->DB2->where('DATE_REQUESTED',$date);
        $this->DB2->where('BOOK_STATUS_ID', 2);
        $this->DB2->update('tbl_borrow_returned');
        $response = array("status" => "Checked!",
                          "MESSAGE"=> "TOTAL AFFECTED ROWS:".$this->db->affected_rows(),
                          "TIME" => $date);

       return json_encode($response);
    }

  }

?>