<?php 
	
	class ApiV1_model extends CI_Model
	{


		public function student($payload)
		{
			header('Content-Type: application/json');
			if(!isset($payload->method)){
			$response = array('status' => 'ERROR',
							  'message' => 'ERROR :P');
			return json_encode($response);
			}

			switch($payload->method){
				
				case "authenticate"	:
					$validate = $this->checkData($payload,"username","birthday","password");
					#echo $validate;
					if(!$validate){
						$response = array('status' => 'ERROR',
							  'message' => 'ERROR :P');
						return json_encode($response);
					}

					$query = $this->db->query("CALL authenticateStudent('".$payload->username."','".base64_encode($payload->password)."')");

					if($query){
						if($query->num_rows() > 0){
							$data = $query->row();
								$response = array('status' => 'SUCCESS',
									 'message' => 'Retrieving Success', 
									 'payload' =>	$data);
							return json_encode($response);
						}else{
								$response = array('status' => 'FAILED',
									  'message' => 'Fail...');
							return json_encode($response);
					
						}
					}else{
							$response = array('status' => 'ERROR',
											  'message' => 'ERROR FETCHING RECORDS');
							return json_encode($response);
					}
					
					break;

				case "student_info"	:
					$validate = $this->checkData($payload,"si_ID");
					#echo $validate;
					if(!$validate){
						$response = array('status' => 'ERROR',
							  'message' => 'ERROR :P');
						return json_encode($response);
					}

					$query = $this->db->query("CALL getInfo('".$payload->method."','".$payload->si_ID."')");

					if($query){
						if($query->num_rows() > 0){
							$data = $query->row();
								$response = array('status' => 'SUCCESS',
									 'message' => 'Retrieving Success', 
									 'payload' =>	$data);
							return json_encode($response);
						}else{
								$response = array('status' => 'FAILED',
									  'message' => 'Fail...');
							return json_encode($response);
					
						}
					}else{
							$response = array('status' => 'ERROR',
											  'message' => 'ERROR FETCHING RECORDS');
							return json_encode($response);
					}
					break;

				case "student_additional_info"	:
					$validate = $this->checkData($payload,"si_ID");
					#echo $validate;
					if(!$validate){
						$response = array('status' => 'ERROR',
							  'message' => 'ERROR :P');
						return json_encode($response);
					}

					$query = $this->db->query("CALL getInfo('".$payload->method."','".$payload->si_ID."')");

					if($query){
						if($query->num_rows() > 0){
							$data = $query->row();
								$response = array('status' => 'SUCCESS',
									 'message' => 'Retrieving Success', 
									 'payload' =>	$data);
							return json_encode($response);
						}else{
								$response = array('status' => 'FAILED',
									  'message' => 'Fail...');
							return json_encode($response);
					
						}
					}else{
							$response = array('status' => 'ERROR',
											  'message' => 'ERROR FETCHING RECORDS');
							return json_encode($response);
					}
					break;

				case "curriculum"	:
					$validate = $this->checkData($payload,"si_ID");
					#echo $validate;
					if(!$validate){
						$response = array('status' => 'ERROR',
							  'message' => 'ERROR :P');
						return json_encode($response);
					}

					$query = $this->db->query("CALL getInfo('".$payload->method."','".$payload->si_ID."')");

					if($query){
						if($query->num_rows() > 0){
							$data = $query->row();
								$response = array('status' => 'SUCCESS',
									 'message' => 'Retrieving Success', 
									 'payload' =>	$data);
							return json_encode($response);
						}else{
								$response = array('status' => 'FAILED',
									  'message' => 'Fail...');
							return json_encode($response);
					
						}
					}else{
							$response = array('status' => 'ERROR',
											  'message' => 'ERROR FETCHING RECORDS');
							return json_encode($response);
					}
					break;

				case "getallsemsofstudent"	:
					$validate = $this->checkData($payload,"si_ID");
					#echo $validate;
					if(!$validate){
						$response = array('status' => 'ERROR',
							  'message' => 'ERROR :P');
						return json_encode($response);
					}

					$query = $this->db->query("CALL getInfo('".$payload->method."','".$payload->si_ID."')");

					if($query){
						if($query->num_rows() > 0){
							$data = $query->result();
								$response = array('status' => 'SUCCESS',
									 'message' => 'Retrieving Success', 
									 'payload' =>	$data);
							return json_encode($response);
						}else{
								$response = array('status' => 'FAILED',
									  'message' => 'Fail...');
							return json_encode($response);
					
						}
					}else{
							$response = array('status' => 'ERROR',
											  'message' => 'ERROR FETCHING RECORDS');
							return json_encode($response);
					}
					break;

				case "enrolled_subject"	:
					$validate = $this->checkData($payload,"si_ID","sem_ID");
					#echo $validate;
					if(!$validate){
						$response = array('status' => 'ERROR',
							  'message' => 'ERROR :P');
						return json_encode($response);
					}

					$query = $this->db->query("CALL getInfoBySem('".$payload->method."','".$payload->si_ID."','".$payload->sem_ID."')");

					if($query){
						if($query->num_rows() > 0){
							$data = $query->result();
								$response = array('status' => 'SUCCESS',
									 'message' => 'Retrieving Success', 
									 'payload' =>	$data);
							return json_encode($response);
						}else{
								$response = array('status' => 'FAILED',
									  'message' => 'Fail...');
							return json_encode($response);
					
						}
					}else{
							$response = array('status' => 'ERROR',
											  'message' => 'ERROR FETCHING RECORDS');
							return json_encode($response);
					}
					break;
					
				case "encoded_grades"	:
					$validate = $this->checkData($payload,"si_ID","sem_ID");
					#echo $validate;
					if(!$validate){
						$response = array('status' => 'ERROR',
							  'message' => 'ERROR :P');
						return json_encode($response);
					}

					$query = $this->db->query("CALL getInfoBySem('".$payload->method."','".$payload->si_ID."','".$payload->sem_ID."')");

					if($query){
						if($query->num_rows() > 0){
							$data = $query->result();
								$response = array('status' => 'SUCCESS',
									 'message' => 'Retrieving Success', 
									 'payload' =>	$data);
							return json_encode($response);
						}else{
								$response = array('status' => 'FAILED',
									  'message' => 'Fail...');
							return json_encode($response);
					
						}
					}else{
							$response = array('status' => 'ERROR',
											  'message' => 'ERROR FETCHING RECORDS');
							return json_encode($response);
					}
					break;

				case "getcurricullum" :
				
					$validate = $this->checkData($payload,"si_ID");
						if(!$validate){
						$response = array('status' => 'ERROR',
							  'message' => 'ERROR :P');
						return json_encode($response);
					}

					$this->db->select('*');
					$this->db->from('student_information si');
					$this->db->join('curricullum_enrolled ce','ce.si_ID = si.si_ID');
					$this->db->join('curricullum_list cl','cl.curricullum_ID = ce.curricullum_ID');		
					$this->db->where('si.si_ID', $payload->si_ID);
					$query = $this->db->get();
					$datacurricullum = $query->row();			
					$querydata = $datacurricullum->curricullum_ID; //curricullum id
					
					$this->db->reconnect();
					$this->db->select('ys.yr_sem_ID, ys.yr_sem_NAME');
					$this->db->from('curricullum_subjects cs');
					$this->db->join('yr_sem ys','ys.yr_sem_ID = cs.yr_sem_ID');
					$this->db->where('curricullum_ID', $querydata);
					$this->db->group_by('cs.yr_sem_ID'); 
					$querysemester = $this->db->get();
					$datasemester = $querysemester->result(); //get semester id

					   foreach ($datasemester as $semid)
           			 {
                 		$semesterId[] = $semid->yr_sem_ID;
                 		$semesterName[] = $semid->yr_sem_NAME;
            		 }
            		  
            		   for($i = 0 ; $i <count($semesterId); $i++)
             			{

					$this->db->reconnect();
					$this->db->select('sl.subject_CODE, sl.subject_DESCRIPTION,(SELECT sll.SUBJECT_CODE FROM subject_list as sll WHERE  sll.subject_ID = cs.prerequisite_subject_ID ) as prerequisitesubj');
					$this->db->from('curricullum_subjects cs');
					$this->db->join('yr_sem ys','ys.yr_sem_ID = cs.yr_sem_ID');
					$this->db->join('subject_list sl','sl.subject_ID = cs.subject_ID');
					$this->db->where('cs.curricullum_ID', $querydata);
					$this->db->where('ys.yr_sem_ID', $semesterId[$i]);	
					$querygetcurricullumsubjects = $this->db->get();

					$datacurricullumsubjects[] = array('Semester' =>$semesterId[$i],
														'SemesterName' => $semesterName[$i],
														'SemesterSubject'=> $querygetcurricullumsubjects->result());
						}				

					if($query){
						if($query->num_rows() > 0){
								$response = array('status' => 'SUCCESS',
									 'message' => 'Retrieving Success', 
									 'payload' =>	array('Curricullum'=> $datacurricullum,
														'CurricullumSubject' => $datacurricullumsubjects));
							
							return json_encode($response);
						}else{
								$response = array('status' => 'FAILED',
									  'message' => 'Fail...');
							return json_encode($response);
					
						}
					}else{
							$response = array('status' => 'ERROR',
											  'message' => 'ERROR FETCHING RECORDS');
							return json_encode($response);
					}

					break;


				case "fetchAudit" :

				$query = $this->db->query("CALL fetchAuditTrail()");

					if($query){
						if($query->num_rows() > 0){
							$data = $query->result();
								$response = array('status' => 'SUCCESS',
									 'message' => 'Retrieving Success', 
									 'payload' =>	$data);
							return json_encode($response);
						}else{
								$response = array('status' => 'FAILED',
									  'message' => 'Fail...');
							return json_encode($response);
					
						}
					}else{
							$response = array('status' => 'ERROR',
											  'message' => 'ERROR FETCHING RECORDS');
							return json_encode($response);
						}
						
					break;


				case "PostAudit" :

				  if($payload == null){
			            $response = array(
			                'status' => 'FAILED',
			                'message' => 'PLEASE CHECK YOUR DATA'
			            );
			            echo json_encode($response);
			        }
			        elseif($payload != null){

				$query = $this->db->query("CALL PostAudit('".$payload->acct_id."', '".$payload->client_ip."','".$payload->event_action."','".$payload->eventdesc."')");
								$response = array(
					                'status' => 'SUCCESS',
					                'message' => 'SUCCESS INSERTING DATA',
					                'payload' => $payload
					            );


						}else
						{

						$response = array(
					                'status' => 'ERROR',
					                'message' => 'ERROR'
					            );
					            
											}
						echo json_encode($response);
						
					break;

				default :
					$response = array('status' => 'ERROR',
							  'message' => 'ERROR :P');
					return json_encode($response);
			}
			
		}


		public function checkData($payload,	...$data){
				$response = true;
				foreach ($data as $datum) {		
					if(!isset($payload->$datum)){
						return false;
					}
			    }
			    return $response;
		}



	}

?>
