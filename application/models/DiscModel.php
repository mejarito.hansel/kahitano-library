<?php 
	
	class DiscModel extends CI_Model
	{


		public function viewAllDisc()
		{
     header('Content-Type: application/json');
     $result = $this->db->query("CALL viewAllDisc()");
     $arr_result = $result->result();
     return $arr_result;
    }

		public function viewStudents()
		{
     header('Content-Type: application/json');
     $result = $this->db->query("CALL viewStudents()");
     $arr_result = $result->result();
     return $arr_result;
    }

    public function addDiscStudent($payload)
    {
      header('Content-Type: application/json');

      $this->db->insert('disc_action', $payload);
      $response = array(
        'status' => 'SUCCESS',
        'message' => 'Success insert into disciplinary_action',
        'payload' => $payload
      );
      echo json_encode($response);
    }

    public function dltDiscStudent($payload)
    {
      if ($payload == null)
      {
        $response = array (
          'status' => 'SUCCESS',
          'message' => 'INSUFFICIENT DATA'
        );
        echo json_encode($response);
      } else if ($payload != null)
      {
        $fetch = $this->db->query("CALL dltDiscStudent('".$payload["DISC_ID"]."')");
        
        $response = array (
          'status' => 'SUCCESS',
          'message' => 'SUCCESS DELETING'
        );
        echo json_encode($response);
      }
      else{
        $response = array(
          'status' => 'FAILED',
          'message' => 'FAILED DELETING'
        );
      }
    }

	}

?>