	<?php
class ApiDormv1_model extends CI_Model

{
	public function Dorm($payload)

	{
		header('Content-Type: application/json');
		if (!isset($payload->method)) {
			$response = array(
				'status' => 'ERROR',
				'message' => 'ERROR :P'
			);
			return json_encode($response);
		}
		switch ($payload->method) {


		case "checkRoomIfexist":

		$validate = $this->checkData($payload, "roomname");

			if (!$validate) {
				$response = array(
					'status' => 'ERROR',
					'message' => 'ERROR :P'
				);
				return json_encode($response);
			}
			$query = $this->db->query("CALL CheckifExist('" . $payload->roomname . "')");

			if($query->num_rows() <= 0 ){

						$response = array(
							'status' => 'AVAILABLE',
							'message' => 'ROOMNAMEAVAILABLE',
							'payload' => $query->result()

						);

					}else{

						$response = array(
								'status' => 'NOTAVAILABLE',
								'message' => 'ROOM NUMBER NOTAVAILABLE'
							); // 
					}
					
					return json_encode($response);
		break;

		case "addnewroom":

			$validate = $this->checkData($payload, "roomname", "capacity", "created_by");
			if (!$validate) {
				$response = array(
					'status' => 'ERROR',
					'message' => 'ERROR :P'
				);
				return json_encode($response);
			}
			if (($payload->roomname == "") && ($payload->capacity == "")) {
				$response = array(
					'status' => 'FAILED',
					'message' => 'ROOMNAME AND CAPACITY IS REQUIRED'
				);
			}
			elseif (($payload == null) || ($payload->capacity == "")) {
				$response = array(
					'status' => 'FAILED',
					'message' => 'CAPACITY IS REQUIRED'
				);
			}
			elseif (($payload == null) || ($payload->roomname == "")) {
				$response = array(
					'status' => 'FAILED',
					'message' => 'ROOMNAME IS REQUIRED'
				);
			}
			elseif (($payload == null) || ($payload->capacity == "0")) {
				$response = array(
					'status' => 'FAILED',
					'message' => 'CANNOT INPUT 0 IN CAPACITY'
				);
			}
			else {
				$query = $this->db->query("CALL AddNewRoom('" . $payload->roomname . "','" . $payload->capacity . "','" . $payload->created_by . "')");
				if ($this->db->trans_status() === FALSE) {
					$response = array(
						'status' => 'ERROR',
						'message' => 'ERROR INSERTING DATA',
						'payload' => $payload
					); // if success
				}
				else {
					$response = array(
						'status' => 'SUCCESS',
						'message' => 'SUCCESS INSERTING DATA',
						'payload' => $payload
					); // if success
				}
			}
			return json_encode($response);

			break;

		case "addstudenttodorm":

			$validate = $this->checkData($payload, "dormid", "si_ID", "created_by");

			if (!$validate) {
				$response = array(
					'status' => 'ERROR',
					'message' => 'ERROR :P'
				);
				return json_encode($response);
			}
			if (($payload->dormid == "") && ($payload->si_ID == "")) {
				$response = array(
					'status' => 'FAILED',
					'message' => 'DORMID AND STUDENTID IS REQUIRED'
				);
			}
			elseif (($payload == null) || ($payload->dormid == "")) {
				$response = array(
					'status' => 'FAILED',
					'message' => 'DORMID IS REQUIRED'
				);
			}
			elseif (($payload == null) || ($payload->si_ID == "")) {
				$response = array(
					'status' => 'FAILED',
					'message' => 'si_ID IS REQUIRED'
				);
			}
			else{

				$query = $this->db->query("CALL AddNewStudenttoRoom('" . $payload->dormid . "','" . $payload->si_ID . "','" . $payload->created_by . "')");

				if ($this->db->trans_status() === FALSE) {
					$response = array(
						'status' => 'ERROR',
						'message' => 'ERROR INSERTING DATA',
						'payload' => $payload
					); // if success
				}
				else {
					$response = array(
						'status' => 'SUCCESS',
						'message' => 'SUCCESS INSERTING DATA',
						'payload' => $payload
					); // if success
				}
				
			}
			return json_encode($response);
			break;


		case "getallenrolledandnodormassigned" :
		
				$query = $this->db->query("CALL `GetAllEnrolled`()");
					$result = $query->result();
				
					if($query->num_rows() < 0 ){

						$response = array(
							'status' => 'NORECORDS',
							'message' => 'NO RECORDS FOUND :P'
						);

					}else{

						$response = array(
								'status' => 'SUCCESS',
								'message' => 'SUCCESS FETCHING DATA',
								'payload' => $result
							); // 
					}

				return json_encode($response);



		break;

		case "transferstudent" :

		$validate = $this->checkData($payload, "dormid","newdormid","si_ID", "last_modified_by");

			if (!$validate) {
				$response = array(
					'status' => 'ERROR',
					'message' => 'ERROR :P'
				);
				return json_encode($response);
			}
			if (($payload->newdormid == "") && ($payload->si_ID == "") && ($payload->dormid == "")&&($payload->last_modified_by == "")) {
				$response = array(
					'status' => 'FAILED',
					'message' => 'OLDDORMID,  NEWDORMID, STUDENTID AND last_modified_by IS REQUIRED'
				);
			}
			elseif (($payload == null) || ($payload->dormid == "")) {
				$response = array(
					'status' => 'FAILED',
					'message' => 'OLDDORMID IS REQUIRED'
				);
			}
			elseif (($payload == null) || ($payload->si_ID == "")) {
				$response = array(
					'status' => 'FAILED',
					'message' => 'si_ID IS REQUIRED'
				);
			}
			elseif (($payload == null) || ($payload->newdormid == "")) {
				$response = array(
					'status' => 'FAILED',
					'message' => 'NEWDORMID IS REQUIRED'
				);
			}
			elseif (($payload == null) || ($payload->last_modified_by == "")) {
				$response = array(
					'status' => 'FAILED',
					'message' => 'Last_Modified_By IS REQUIRED'
				);
			}else{

				$this->db->query("UPDATE dorm SET available_slot = available_slot + 1 WHERE dorm_id = '".$payload->dormid."' ");
				$this->db->query("UPDATE dorm SET available_slot = available_slot - 1 WHERE dorm_id = '".$payload->newdormid."' ");
				$this->db->query("UPDATE dorm_students SET dorm_id = '".$payload->newdormid."' WHERE si_ID = '".$payload->si_ID."'");
				$response = array(
					'status' => 'SUCCESS',
					'message' => 'SUCCESS UPDATING'
				);

			}


			return json_encode($response);
		
		break;


		case "checkifavailable":

				$query = $this->db->query("CALL `GetAllDorm`()");
					$result = $query->result();
				
					if($query->num_rows() <= 0 ){

						$response = array(
							'status' => 'NORECORDS',
							'message' => 'NO RECORDS FOUND :P'
						);

					}else{

						$response = array(
								'status' => 'SUCCESS',
								'message' => 'SUCCESS FETCHING DATA',
								'payload' => $result
							); // 
					}

				return json_encode($response);

		break;



		case "getStudentDorm":
		
			$validate = $this->checkData($payload, "dormid");

			if (!$validate) {
				$response = array(
					'status' => 'ERROR',
					'message' => 'ERROR :P'
				);
				return json_encode($response);
			}

			if (($payload == null) || ($payload->dormid == "")) {
			
				$response = array(
					'status' => 'FAILED',
					'message' => 'DORMID IS REQUIRED'
				);

			}else{

				$query = $this->db->query("CALL `getStudentDorm`('" . $payload->dormid . "')");
					$result = $query->result();
				
					if($query->num_rows() < 0 ){

						$response = array(
							'status' => 'NORECORDS',
							'message' => 'NO RECORDS FOUND :P'
						);

					}else{

						$response = array(
								'status' => 'SUCCESS',
								'message' => 'SUCCESS FETCHING DATA',
								'payload' => $result
							); // 
					}

				}

				return json_encode($response);

		break;

		case "removestudentfromdorm" :
			
		$validate = $this->checkData($payload, "dormid","si_ID");

			if (!$validate) {
				
				$response = array(
					'status' => 'ERROR',
					'message' => 'ERROR :P'
				);

				return json_encode($response);
			}

			if (($payload == null) || ($payload->dormid == "")) {
			
				$response = array(
								'status' => 'FAILED',
								'message' => 'DORMID IS REQUIRED'
							);

			}elseif(($payload == null) || ($payload->si_ID == "")){

				$response = array(
								'status' => 'FAILED',
								'message' => 'si_ID IS REQUIRED'
							);

			}else{
								$rec = explode(',', $payload->si_ID);
								$count=count($rec);
								$this->db->query("UPDATE dorm SET available_slot = available_slot + ".$count." WHERE dorm_id = '".$payload->dormid."' ");
								// $this->db->query("UPDATE dorm_students SET status = '2' WHERE si_ID IN ('".$rec."'') AND  dorm_id = '".$payload->dormid."' ");
								$this->db->set('status', '2');
								$this->db->where('dorm_id', $payload->dormid);
								$this->db->where_in('si_ID', $rec);
								$this->db->update('dorm_students');

				$response = array(
								'status' => 'SUCCESS',
								'message' => 'SUCCESSFULLY UPDATED RoomAvailability',
								'payload' => $payload);

			}

			return json_encode($response);
	
		break;

		case "UpdateRoomAvailability":

		$validate = $this->checkData($payload, "dormid","student_out");

			if (!$validate) {
				
				$response = array(
					'status' => 'ERROR',
					'message' => 'ERROR :P'
				);

				return json_encode($response);
			}

			if ($payload == null) {
				$result = array(
					'result' => 'ERROR',
					'message' => 'ERROR PLEASE CHECK YOUR DATA'
				);
				echo json_encode($result);
			}
			elseif ($payload != null) {
				$this->db->query("CALL UpdateRoomAvailability('" . $payload->dormid . "','" . $payload->student_out . "')");
				$result = array(
					'status' => 'SUCCESS',
					'message' => 'SUCCESSFULLY UPDATED RoomAvailability',
					'payload' => $payload
				);
			}
			else {
				$result = array(
					'status' => 'ERROR',
					'message' => 'Fill out all the required parameters',
					'required_params' => ''
				);
			}

			echo json_encode($result);


		break;


		default:
			$response = array(
				'status' => 'ERROR',
				'message' => 'ERROR :P'
			);
			return json_encode($response);
		}
	}
	
	public function checkData($payload,	...$data){
				$response = true;
				foreach ($data as $datum) {		
					if(!isset($payload->$datum)){
						return false;
					}
			    }
			    return $response;
		}
	
	public function UpdateRoomAvailability($payload)
	{
		if ($payload == null) {
			$result = array(
				'result' => 'ERROR',
				'message' => 'ERROR PLEASE CHECK YOUR DATA'
			);
			echo json_encode($result);
		}
		elseif ($payload != null) {
			$this->db->query("CALL UpdateRoomAvailability('" . $payload->dormid . "','" . $payload->student_out . "')");
			$result = array(
				'status' => 'SUCCESS',
				'message' => 'SUCCESSFULLY UPDATED RoomAvailability',
				'payload' => $payload
			);
		}
		else {
			$result = array(
				'status' => 'ERROR',
				'message' => 'Fill out all the required parameters',
				'required_params' => ''
			);
		}

		echo json_encode($result);
	}
}
?>